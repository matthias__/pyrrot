"""
This module test the module lib.wallpaper for errors. It uses pytest, and will
be run as part of a coverage run -m pytest --junitxml=report.xml for instance.
"""

import unittest

class TestWallpaper(unittest.TestCase):
    """
    This is the class to test the functions of the module lib.wallpaper.
    """

    pass
